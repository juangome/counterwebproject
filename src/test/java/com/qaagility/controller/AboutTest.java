package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class AboutTest {
	@Test
	public void testDesc() throws Exception {
		String res = new About().desc ();
		assertEquals("bla","This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!",res);
	}
}
